//
//  JOEBlurEffectView.m
//  UITest
//
//  Created by Joe Lee on 2014-12-8.
//  Copyright (c) 2014年 Joe. All rights reserved.
//

#import "JOEBlurEffectView.h"

#define IS_SYSTEM_NEWER_OR_SAME_TO(version) ([[[UIDevice currentDevice] systemVersion] floatValue] >= version)


@interface JOEBlurEffectView ()

@property (nonatomic, strong) UIView *blurBackgroundView;

@end


@implementation JOEBlurEffectView

- (instancetype)initWithFrame:(CGRect)frame
{
  if (self = [super initWithFrame:frame]) {
    [self blurBackgroundView];
  }

  return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
  if (self = [super initWithCoder:aDecoder]) {
    [self blurBackgroundView];
  }

  return self;
}

- (instancetype)init
{
  if (self = [super init]) {
    [self blurBackgroundView];
  }

  return self;
}

#pragma mark - setters & getters

- (void)setBlurEffectStyle:(JOEBlurEffectStyle)blurEffectStyle
{
  if (_blurEffectStyle == blurEffectStyle) {
    return;
  }

  _blurEffectStyle = blurEffectStyle;
  if (IS_SYSTEM_NEWER_OR_SAME_TO(8)) {
    [self.blurBackgroundView removeFromSuperview];
    _blurBackgroundView = nil;
    [self blurBackgroundView];
  } else {
    [(UIToolbar *)self.blurBackgroundView setBarStyle:(JOEBlurEffectStyleDark == self.blurEffectStyle) ? UIBarStyleBlack : UIBarStyleDefault];
    [(UIToolbar *)self.blurBackgroundView setTranslucent:YES];
  }
}

- (UIView *)blurBackgroundView
{
  if (_blurBackgroundView) {
    return _blurBackgroundView;
  }

  [self setBackgroundColor:[UIColor clearColor]];

  if (IS_SYSTEM_NEWER_OR_SAME_TO(8)) {
    UIVisualEffect *visualEffect = [UIBlurEffect effectWithStyle:(UIBlurEffectStyle)self.blurEffectStyle];
    _blurBackgroundView = [[UIVisualEffectView alloc] initWithEffect:visualEffect];
  } else {
    _blurBackgroundView = [[UIToolbar alloc] initWithFrame:CGRectZero];
    [(UIToolbar *)_blurBackgroundView setBarStyle:(JOEBlurEffectStyleDark == self.blurEffectStyle) ? UIBarStyleBlack : UIBarStyleDefault];
    [(UIToolbar *)_blurBackgroundView setTranslucent:YES];
  }

  _blurBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
  [self addSubview:_blurBackgroundView];
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[_blurBackgroundView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_blurBackgroundView)]];
  [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[_blurBackgroundView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_blurBackgroundView)]];

  return _blurBackgroundView;
}

@end