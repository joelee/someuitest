//
//  JOEBlurEffectView.h
//  UITest
//
//  Created by Joe Lee on 2014-12-8.
//  Copyright (c) 2014年 Joe. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM (NSInteger, JOEBlurEffectStyle) {
  JOEBlurEffectStyleExtraLight,
  JOEBlurEffectStyleLight,
  JOEBlurEffectStyleDark
};


@interface JOEBlurEffectView : UIView

@property (nonatomic, assign) JOEBlurEffectStyle blurEffectStyle; // Default is JOEBlurEffectStyleExtraLight

@end