//
//  Object+Private.m
//  UITest
//
//  Created by Joe Lee on 2014-9-23.
//  Copyright (c) 2014年 Joe. All rights reserved.
//

#import "Object+Private.h"

@implementation Object (Private)

- (void)setStringPrivate:(NSString *)string
{
  [self setValue:string forKey:@"string"];
}

@end