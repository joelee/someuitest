//
//  SubView.m
//  UITest
//
//  Created by Joe Lee on 2014-8-11.
//  Copyright (c) 2014年 Joe. All rights reserved.
//

#import "SubView.h"

@implementation SubView

- (instancetype)initWithFrame:(CGRect)frame
{
  if (self = [super initWithFrame:frame]) {
    self.backgroundColor = [UIColor greenColor];
  }
  return self;
}

- (void)layoutSubviews
{
  [super layoutSubviews];
}

@end