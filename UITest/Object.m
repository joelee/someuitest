//
//  Object.m
//  UITest
//
//  Created by Joe Lee on 2014-9-23.
//  Copyright (c) 2014年 Joe. All rights reserved.
//

#import "Object.h"

@interface Object ()

@property (nonatomic, strong) NSString *string;

@end


@implementation Object

- (BOOL)isEqual:(id)object
{
  return (self.string == [(Object *)object string]);
}

//- (NSUInteger)hash
//{
//  return self.string.hash;
//}

- (void)printString
{
  NSLog(@"%@", self.string);
}

- (void)setString:(NSString *)string
{
  if (_string == string) {
    return;
  }

  [self willChangeValueForKey:@"string"];
  _string = string;
  [self didChangeValueForKey:@"string"];
}

@end