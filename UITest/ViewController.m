//
//  ViewController.m
//  UITest
//
//  Created by Joe Lee on 2014-8-11.
//  Copyright (c) 2014年 Joe. All rights reserved.
//

#import "ViewController.h"

#import <AssetsLibrary/AssetsLibrary.h>

#import "SubView.h"
#import "Object+Private.h"
#import "ILTranslucentView.h"
#import "FXBlurView.h"
#import "JOEBlurEffectView.h"


@interface ViewController ()

@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIButton *button;

@property (nonatomic, assign) BOOL hiddenNaviBar;

@end

static int vi = 0;

@implementation ViewController
            
- (void)viewDidLoad
{
  [super viewDidLoad];

  [self.view setBackgroundColor:[UIColor whiteColor]];

  UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
  [button setFrame:CGRectMake(self.view.frame.size.width - 100, self.view.frame.size.height - 200, 100, 44)];
  [button setTitle:@"break point" forState:UIControlStateNormal];
  [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
  [button addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
  [self.view addSubview:button];

  [self.view addSubview:self.imageView];
  [self.view addSubview:self.button];

  self.navigationItem.title = [NSString stringWithFormat:@"%d", vi++];
  self.hiddenNaviBar = (vi/3)%2;
}

- (void)viewWillAppear:(BOOL)animated
{
  [super viewWillAppear:animated];
  [self.navigationController setNavigationBarHidden:self.hiddenNaviBar animated:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
  [super viewDidAppear:animated];
}

- (void)didEffectViewLongPressed:(id)sender
{
  [self dismissViewControllerAnimated:YES completion:^{}];
}

- (void)didEffectViewTapped:(id)sender
{
  JOEBlurEffectView *effectView = (JOEBlurEffectView *)[sender view];
  JOEBlurEffectStyle blurEffectStyle = effectView.blurEffectStyle;
  blurEffectStyle = ((blurEffectStyle + 1) % 3);
  [effectView setBlurEffectStyle:blurEffectStyle];
}

- (void)buttonTapped:(id)sender
{
  UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
  imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
  [self presentViewController:imagePickerController animated:YES completion:^{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{

      // UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleLight]];
      // ILTranslucentView *effectView = [[ILTranslucentView alloc] initWithFrame:CGRectZero];
      // UIToolbar *effectView = [[UIToolbar alloc] initWithFrame:CGRectZero];
      // UIView *effectView = [[NSClassFromString(@"_UIBackdropEffectView") alloc] initWithFrame:CGRectZero];
      // FXBlurView *effectView = [[FXBlurView alloc] initWithFrame:CGRectZero];
      JOEBlurEffectView *effectView = [[JOEBlurEffectView alloc] initWithFrame:CGRectZero];

      // effectView.blurEnabled = YES;
      // effectView.dynamic = YES;
      // effectView.updateInterval = -1;

      // effectView.barStyle = UIBarStyleBlack;
      // effectView.translucent = YES;

      // effectView.translucentAlpha = 1;
      // effectView.translucentStyle = UIBarStyleBlack;

      CGRect frame = [UIApplication sharedApplication].keyWindow.frame;
      frame.origin.x = 0;
      frame.origin.y = [UIApplication sharedApplication].keyWindow.frame.size.height;
      [effectView setFrame:frame];
      frame.origin.y = 0;
      [imagePickerController.view addSubview:effectView];

      effectView.userInteractionEnabled = YES;
      UIGestureRecognizer *longPressGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(didEffectViewLongPressed:)];
      [effectView addGestureRecognizer:longPressGesture];
      UIGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didEffectViewTapped:)];
      [effectView addGestureRecognizer:tapGesture];

      [UIView animateWithDuration:1 animations:^{
        effectView.frame = frame;
      } completion:^(BOOL finished) {
        effectView.frame = frame;
        // UIView *subview = [[[effectView subviews].firstObject subviews].firstObject subviews].firstObject;
        // subview.hidden = YES;
        UIView *test = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 10, 10)];
        [test setBackgroundColor:[UIColor redColor]];
        [effectView addSubview:test];
      }];
    });
  }];

  return;

  dispatch_semaphore_t t = dispatch_semaphore_create(0);
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
    ALAssetsLibrary *assetsLibrary = [[ALAssetsLibrary alloc] init];
    [assetsLibrary enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
      if (!group || *stop) {
        dispatch_semaphore_signal(t);
        *stop = YES;
        return;
      }
      [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if ([[(NSURL *)[result valueForProperty:ALAssetPropertyAssetURL] absoluteString] isEqualToString:@"assets-library://asset/asset.JPG?id=DFBA6770-D974-44B1-BA3F-D0B1DBE30F6B&ext=JPG"]) {
          UIImage *image = [UIImage imageWithCGImage:result.aspectRatioThumbnail];
          NSLog(@"%@", image);
        }
      }];

      [group enumerateAssetsWithOptions:NSEnumerationReverse usingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if (result) {
          UIImage *image = [UIImage imageWithCGImage:result.thumbnail];
          NSLog(@"%@", image);
          *stop = YES;
        }
      }];
      NSLog(@"");
//      [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
//        NSString *localIdentifier = [result.defaultRepresentation.url absoluteString];
//        NSLog(@"%@", localIdentifier);
//        [assetsLibrary assetForURL:[NSURL URLWithString:localIdentifier] resultBlock:^(ALAsset *asset) {
//          NSLog(@"Y %@", localIdentifier);
//        } failureBlock:^(NSError *error) {
//          NSLog(@"N %@", localIdentifier);
//        }];
//      }];
    } failureBlock:^(NSError *error) {
      dispatch_semaphore_signal(t);
    }];
  });
  dispatch_semaphore_wait(t, DISPATCH_TIME_FOREVER);

  NSMutableArray *array = [NSMutableArray array];

  Object *object0 = [[Object alloc] init];
  [object0 setStringPrivate:@"sadfwef"];
  [object0 printString];

  Object *object1 = [[Object alloc] init];
  [object1 setStringPrivate:@"sadfwef"];
  [object1 printString];

  [array addObject:object0];
  BOOL value = YES;
  value = [array containsObject:object1];

  [array removeObject:object1];

  NSLog(@"");
}

- (UIImageView *)imageView
{
  if (_imageView) {
    return _imageView;
  }

  _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pic"]];
  // _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 30, 150, 150)];
  [_imageView setImage:[UIImage imageNamed:@"pic"]];
  [_imageView setBackgroundColor:[UIColor orangeColor]];
  return _imageView;
}

- (UIButton *)button
{
  if (_button) {
    return _button;
  }

  _button = [UIButton buttonWithType:UIButtonTypeCustom];
  [_button setFrame:CGRectMake(10, 200, 150, 150)];
  [_button setImage:[UIImage imageNamed:@"pic"] forState:UIControlStateNormal];
  [_button setBackgroundColor:[UIColor orangeColor]];
  return _button;
}

@end