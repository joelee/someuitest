//
//  NavigationController.m
//  UITest
//
//  Created by Joe Lee on 2014-10-11.
//  Copyright (c) 2014年 Joe. All rights reserved.
//

#import "NavigationController.h"

@interface NavigationController () <UIGestureRecognizerDelegate>

@end

@implementation NavigationController

- (void)viewDidLoad
{
  [super viewDidLoad];

  // self.interactivePopGestureRecognizer.delegate = self;
  // self.interactivePopGestureRecognizer.enabled = YES;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
  return YES;
}

@end
