//
//  Object.h
//  UITest
//
//  Created by Joe Lee on 2014-9-23.
//  Copyright (c) 2014年 Joe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Object : NSObject

- (void)printString;

@end